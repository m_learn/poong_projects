'''
== 1월 ==
=== 1주차(1/1~1/7) ===

 * 1일: 휴방

 * 2일: [[https://torfi.itch.io/the-professional|The Professional]], [[Escape from Tarkov]]

 * 3일: [[Escape from Tarkov]]
format '''
import datetime as dt
import calendar

def extract_weekdays(x):
    '''
    0 -> 6
    1 -> 5
    2 -> 4
    3 -> 3
    4 -> 2
    5 -> 1
    6 -> 7
    '''
    last_day = calendar.monthrange(x.year, x.month)[1]
    start_day = x.day
    if x.weekday() != 6:
        generate_number = 6 - x.weekday()
    else:
        generate_number = 7
    weekdays = []
    for i in range(0,generate_number):
        if not i+start_day > last_day:
            weekdays.append(i+start_day)
    return weekdays

def write_wiki(x, nth_week, wiki_text):
    weekdays = extract_weekdays(x)
    wiki_text += f'=== {nth_week}주차({x.month}/{weekdays[0]} ~ {x.month}/{weekdays[-1]}) ===\n\n'
    daily = [' * ' + str(d) + '일 : ' for d in weekdays]
    for d in daily:
        wiki_text += d + '\n\n'

    return wiki_text
    
 
x = dt.datetime(2023, 1, 1)
d = dt.timedelta(days=1)
wiki_text = ''
for i in range(0,365):
    if x.day == 1:
        nth_week = 1
        wiki_text += f'== {x.month}월 ==\n\n'
        wiki_text = write_wiki(x, nth_week, wiki_text)
    elif x.day > 1 and x.weekday() == 6:
        nth_week +=1
        wiki_text = write_wiki(x, nth_week, wiki_text)
    x+=d

print(wiki_text)